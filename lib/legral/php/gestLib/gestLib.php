<?php
/*!******************************************************************
fichier: gestLib.php
auteur : Pascal TOLEDO
date de creation: 01 fevrier 2012
date de modification: 08 juin 2014
source: https://git.framasoft.org/legraLibs/gestlib
tuto:   https://legral.fr/intersites/lib/php/gestLib
depend de:
	* aucune
description:
	* 
*******************************************************************/
define ('GESTLIBVERSION','2.1.1-dev');
define ('SCRIPTDEBUT', microtime(TRUE));

if (!defined('GESTLIB_OUTPUT')){
    define('GESTLIB_OUTPUT',NULL);
}

// ***************** //
//  ** intersites ** //
// ***************** //

// -- Calcul du reseau -- //

//echo phpinfo();
define('SERVER_NAME',$_SERVER['SERVER_NAME']);
define('SERVER_ADDR',$_SERVER['SERVER_ADDR']);
$SERVER_ADDR=substr($_SERVER['SERVER_ADDR'],0,3);
if   (($SERVER_ADDR === '127')OR($SERVER_ADDR=='::1')){define('SERVER_RESEAU','LOCALHOST');}
elseif($SERVER_ADDR === '192'){define('SERVER_RESEAU','LAN');}
elseif($SERVER_ADDR === '10.'){define('SERVER_RESEAU','VPN');}
else  {define('SERVER_RESEAU','INTERNET');}
unset($SERVER_ADDR);


// =========================== //
// = fonctions independantes = //
// =========================== //

function nl2br2($string) {
    return str_replace(array("\r\n", "\r", "\n"), "<br />", $string);
} 

function br2nl($string){
    return preg_replace('/\<br(\s*)?\/?\>/i', "\n", $string);
}

//*************************************** //
// Gestion de textes multilangues pouvant //
// servir de gestionnaire d'erreurs       //
//*************************************** //
class gestTextes{
	private $langDefaut;
	private $lang;	// langue en cours
	private $no;	//index (numeric ou alphanumerique) du texte (ou de l'erreur)
	private $lastNo;//dernier no setter
	public $textes=array();//contient le tableau des langues

	function __construct($langDefaut='fr'){
		$this->langDefaut=$langDefaut;
		$this->lang=$this->langDefaut;
		$this->no=0;
		$this->lastNo=0;
		$this->textes=array();
	}

	function __destruct(){
	}

	function getLangue(){return $this->lang;}

	// - $force: force la creation - //
	function setLangue($lang='fr',$force=0){
		if( (!isset($this->textes[$lang])) OR ($force===1)){
			$this->textes[$lang]=array();
		}
		$this->lang=$lang;
	}

	function setNo($nu){$this->no=$nu;$this->lastNo=$nu;}
	function getNo(){return $this->no;}
	function getLastNo(){return $this->lastNo;}

	function setTexte($nu,$txt,$lang=NULL,$force=0){
		// - attention le tableau peut ne pas exister ! - //
		$lang=($lang===NULL)?$this->langDefaut:$lang;
		if( (!isset($this->textes[$lang][$nu])) OR ($force===1)){
			$this->textes[$lang][$nu]=$txt;
		}
	}

	function getTexte($nu=NULL,$lang=NULL){
		$nu=($nu===NULL)?$this->lastNo:$nu;
		$lang=($lang===NULL)?$this->langDefaut:$lang;
		if(isset($this->textes[$lang][$nu]))return $this->textes[$lang][$nu];
		return '';
	}
} // class gestTextes


// **************************** //
// CLASS de gestion d'attributs //
// **************************** //
class gestAttributs{
	//private $attrs;
	public $attrs;
	function __construct(){
	$this->attrs=array();
	}

	function set($key,$val,$force=1){
		if((!isset($this->attrs[$key])OR($force===1))){$this->attrs[$key]=$val;}
	}
	function _unset($key){
		//if(!(isset($this->attrs[$key])OR($force===1)))$this->attrs[$key]=$val;
		unset($this->attrs[$key]);
	}

	function get($key=NULL){
		if($key===NULL)return $this->attrs;
		if(isset($this->attrs[$key]))return $this->attrs[$key];
		return NULL;
	}


}

// **************************** //
// CLASS DE GESTION DES ERREURS
// **************************** //
//if (gettype($gestLib)=='NULL')	// protection gestLib
//{
class LEGRALERR{
	const ALWAYS=-1;//sera toujours afficher quelque soit le niveau atrtribuer pour la lib
	const NOERROR=0;
	const CRITIQUE=1;
	const DEBUG=2;
	const WARNING=3;
	const INFO=4;
	const ALL=5;
}
function LEGRALERR_toString($err){
	switch ($err){
		case LEGRALERR::NOERROR :return 'NOERROR';
		case LEGRALERR::CRITIQUE :return 'CRITIQUE';
		case LEGRALERR::DEBUG :return 'DEBUG';
		case LEGRALERR::WARNING :return 'WARNING';
		case LEGRALERR::INFO :return 'INFO';
		case LEGRALERR::ALWAYS :return 'ALWAYS';
	}
}

//********************************************
// CLASS DE GESTION DES ETATS
//********************************************
class LEGRAL_LIBETAT{
	const NOLOADED=0;
	const LOADING=1;
	const LOADED=2;
}
function LEGRAL_LIBETAT_toString($etat){
	switch ($etat)
		{
		case LEGRAL_LIBETAT::NOLOADED :return 'NOLOADED';
		case LEGRAL_LIBETAT::LOADING :return 'LOADING';
		case LEGRAL_LIBETAT::LOADED :return 'LOADED';
		}
}


// ********************************** //
// Class gestOut() 
// - gestion de la sortie de debug - //
// ********************************** //
class GestOut{

    // - device de sortie - //
    //private $isScreen=0;
    private $isReturn=0;
    private $isFile=0;

    // - 1-desactiver 0:ne pas changer 1:activer 1 fois le device - //
    //private $isScreenOnce=0;
    private $isReturnOnce=0;
    private $isFileOnce=0;
    
    private $logPath=NULL; // chemin utiliser par le device fichier 
    private $fh; // file handler

    function __construct($logPath=NULL){
        @mkdir (dirname($logPath));
        $this->logPath=($this->fh=@fopen($logPath,'w'))?$logPath:NULL;
    }

    function __destruct(){
        $this->stop();
    }

    function __toString(){
        $out='GestOut:<br>';
        $out.='isReturn='.$this->isReturn.'<br>';
        $out.='isFile='.$this->isFile.'<br>';
        $out.='logPath='.$this->logPath.'<br>';
        return $out;
    }

    // - hydratation - //
    //function setIsScreen($val=1){$this->isScreen=((int)$val===0)?0:1;}
    //function setIsScreenOnce($val=1){$this->isScreenOnce=((int)$val===0)?0:1;}

    function setIsReturn($val=1){$this->isReturn=((int)$val===0)?0:1;}
    // -1:desactivation temporaire; 1: activation temporaire
    function setIsReturnOnce($val=1){$this->isReturnOnce=((int)$val===-1)?-1:1;}

    function setIsFile($val=1){$this->isFile=((int)$val===0)?0:1;}
    function setIsFileOnce($val=1){$this->isFileOnce=((int)$val===-1)?-1:1;}

    function setLogPath(){
        @mkdir (dirname($logPath));
        $this->logPath=($this->fh=@fopen($logPath,'w'))?$logPath:NULL;
    }

    function writeHeader($txt){
        //echo __FUNCTION__.':'.__LINE__.'txt='.$txt.'<br>';

        $this->add($txt);
    }
    function writeDate(){
        $this->add(date('d/m/Y H:i:s').'<br>');
    }

    // ajoute du texte dans la sortie
    function add($txt){
    
        //echo __FUNCTION__.':'.__LINE__.'txt='.$txt.'<br>';

        // - device: file - //
        // desactivation temporaire demander?
        if ($this->isFileOnce===-1){
            $this->isFileOnce=0;
            // nop
        }
        // activation permanente ou temporaire?
        elseif (($this->isFile===1 OR $this->isFileOnce===1) AND $this->logPath !== NULL){
            $this->isFileOnce=0;
            fwrite($this->fh,$txt);
        }


        // - device: return - //
        // desactivation temporaire demander?
        if ($this->isReturnOnce===-1){
            $this->isReturnOnce=0;
            // nop
        }
        // activation permanente ou temporaire?
        elseif ($this->isReturn===1 OR $this->isReturnOnce===1){
            $this->isReturnOnce=0;
            return $txt;
        }
    }

    function stop(){
        if ($this->fh !== NULL){fclose($this->fh);}
    }
}
$gestOut=new GestOut(VAR_ROOT.'debug/'.PROJET_NOM.'.html');
//$gestOut->setIsFile();
//$gestOut->writeHeader('<link rel="stylesheet" href="/git/sites/lib/legral/php/gestLib/gestLib.css">');
//$gestOut->writeDate();


function gestOut_hr(){
    global $gestOut;
    return $gestOut->add('<hr>');
}

function gestOut_file($txt){
    global $gestOut;
    return $gestOut->add('<div class="gestOutFile">'.$txt.'</div>');
}

function gestOut_function($txt){
    global $gestOut;
    return $gestOut->add('<div class="gestOutFunction">'.$txt.'</div>');
}

function gestOut_warn($txt){
    global $gestOut;
    return $gestOut->add('<div class="gestOutWarn">'.$txt.'</div>');
}

function gestOut_info($txt){
    global $gestOut;
    return $gestOut->add('<div class="gestOutInfo">'.$txt.'</div>');
}



//********************************************
// function gestLib_format()
// -  formate les fonctions gestLib_inspect, debugShow(),debugShowVar()
//********************************************
function gestLib_format($txt,$f='br'/*,$classe=''*/){
	switch($f){
		case '':case 'nobr':case NULL:return $txt;break;
		case 'ln':	$o="$txt\n";break;
//		case 'div':	$o="<div class='$classe'>$txt</div>";break;
//		case 'p':	$o="<p class='$classe'>$txt.</p>";break;
		case 'br':default: $o="$txt<br />";
		}
    return $o;
}


//********************************************
// function gestLib_inspect() 
// - manipuler des variables independantes des libs - //
// - retourne les donnees d'une variable independante (nom,$var) au couleur de gestLib.css - //
// - si $varNom='' alors valeur seul - //
// - line,method: voir les info de debuggage - //
// http://php.net/manual/fr/language.constants.predefined.php
//********************************************
function gestLib_inspectOrigine($varNom,$var,$commentaire='',$metaDatas='',$f='br'/*,$classe=''*/){
    global $gestOut;
    $gestOut->setIsReturn(1);
    $gestOut->setIsFileOnce(-1);
    $out='<span class="gestLibVar">'.$commentaire.':'.$metaDatas.':'.$varNom.':</span>'
	.	'<div class="gestLib_inspectOrigine">'.gestLib_inspect($varNom,$var,$commentaire='',$metaDatas='',$f='br').'</div>';
    return $gestOut->add($out);
}


//function gestLib_inspect
 function gestLib_inspect($varNom,$var,$commentaire=0,$metaDatas='',$f='br'){
    global $gestOut;
    $out='';
	$out.='<span class="gestLib_inspect">';
	$t=gettype($var);
        if ($metaDatas!=''){$out.="[<span class='gestLibInfo'>$metaDatas</span>]";}
        if ($commentaire!==0){$out.="<span class='gestLibInfo'>:$commentaire</span>&nbsp;";}
        if($varNom!='')$out.='<span class="gestLibVar">'.$varNom.'</span>='; // pas de nom preciser

	$t=gettype($var);

	//$out.='<span class="gestLibVal">';
	$out.="[$t]";

	switch($t){
		case'boolean': case'integer': case'double':
		        if     ($var===TRUE ) {$out.=' <span class="gestLibVal">TRUE strict</span>';}
                elseif ($var===FALSE) {$out.=' <span class="gestLibVal">FALSE strict</span>';}
                else {$out.=" <span class='gestLibVal'>$var</span>";}
			break;

        case'string':
			$out.='<span class="gestLibVal">'.$var.'</span>';break;

        case'NULL':
			$out.='<span class="gestLibValSpecial">NULL</span>';break;

        case'ressource':
			$out.='';break;

        case'array':
			$nb=count($var);
			if($nb>0){
				$out.="(array:$nb)<ul class='gestLibArray'>";
				foreach($var as $key => $value)$out.='<li style="list-style-type:none">'.gestLib_inspect($key,$value).'</li>';
				}
			$out.='</ul>';
                        break;

		case'object':
			$out.='(object:'.count($var).')<ol class="gestLibObject">';
			/// if(get_class_vars($var))$out.='.';	// renvoie les valeurs par defaut // err500
			if(!get_class_methods($var))$out.='<span class="gestLibValSpecial">Classe sans methode.</span>';

			foreach($var as $key => $value)$out.='<li style="list-style-type:none">'.gestLib_inspect($key,$value).'</li>';
			$out.='</ol>';
			break;

        case'unknow type':
			$out.='<span class="gestLibValSpecial">unknow type</span>';break;	

        default:
            $out.='(autre:'.$t.'):'.$var;
            break;
	}

	if (empty($var)){$out.=' <span class="gestLibValSpecial">empty</span> ';}
	$out.='</span>';
	//$out.='</span><!--span class="gestLib_inspect"-->';
    $out=gestLib_format($out,$f/*,$classe*/);

    //echo __FUNCTION__.':'.__LINE__.'out='.$out.'<br>';

    return $gestOut->add($out);
}



//********************************************
// CLASS DE DONNEES
//********************************************
class gestLib_gestLib{
	public $nom=NULL;
	public $fichier=NULL;
	public $version=NULL;

	public $auteur='fenwe';
	public $site='http://legral.fr';
	public $git='https://git.framasoft.org/u/fenwe';
	public $description='';

	public $dur=0;
        public $deb=0;
        public $fin=0;
	public $etat=NULL;	//1: en cours de chargement;2: chargement terminer

	public $err_level=0;
	public $err_level_Backup=NULL;	//sauvegarde de l'etat

	function setEtat($etat){$this->etat=$etat;}
	function getDuree(){return number_format($this->dur,6);}

	function end(){
		$this->etat=LEGRAL_LIBETAT::LOADED;
		$this->fin=microtime(1);$this->dur=$this->fin - $this->deb;
	}

	function setErr($err){$this->err_level=$err;}
	function setErrLevelTemp($err){$this->err_level_Backup=$this->err_level;$this->err_level=$err_level;}
	function setErrLevelTemp_NOERROR(){$this->err_level_Backup=$this->err_level;$this->err_level=LEGRALERR::NOERROR;}
	function restoreErrLevel(){$this->err_level=$this->err_level_Backup;}


	function __construct($nom,$file,$version,$description=NULL){
		$this->deb=microtime(1);
		$this->err_level=LEGRALERR::NOERROR;
		$this->nom=$nom;
		$this->fichier=$file;
		$this->version=$version;
		$this->etat=1;
		$this->description=$description;
	}


	//function __toString() {return '';}

	// - renvoie le texte si le niveau d'erreur concorde - //
	function debugShow($level,$commentaire='',$metaDatas='',$txt='',$format='br',$classe=''){
		if($level>$this->err_level){return NULL;}
		$out='<span class="gestLibVar">'.$this->nom;
		$out.='{'.LEGRALERR_toString($level).'}';
		if ($commentaire!==''){$out.="($commentaire)";}
		if ($metaDatas!==''){$out.="[$metaDatas]";}
		$out.=':</span>';
		$out.='<span class="gestLibVal">'."$txt</span>";
		return gestLib_format($out,$format,$classe);
	}

	
	function debugShowVar($level,$commentaire,$metaDatas,$varNom,$var,$format='br',$classe=''){
		if ($level>$this->err_level){return '';}
		$out=gestLib_inspect($varNom,$var,$commentaire,$metaDatas,$format,$classe);
		return $out;
	}
} // class gestLib_gestLib


//********************************************
// CLASS GESTIONLIBRAIRIE
//********************************************
class gestionLibrairies{
	public $libs=array();	//tableau de libs
	public $erreurs;

	function __construct(){
		$this->erreurs=array();
	}


	function __toString(){
		$out='<ul class="gestlib">';
		foreach($this->lib  as $key => $value)  $out.="<li>$key= $value</li>";$out.="</ul></li>";
		$out.='</ul>';
		return $out;
	}
	

	function loadLib($nom,$file,$version,$description=NULL){
	//	if ( isset($this->libs[$nom]->nom) ){$this->libs[$nom]->nom=NULL;}//si deja charge

		$this->libs["$nom"]=new gestLib_gestLib($nom,$file,$version,$description);
		$this->erreurs["$nom"]=new gestTextes();
	}


	function tableau(){
		$out ='<table class="gestLib"><caption>Librairies PHP</caption>';
		$out.='<thead><tr><th>nom</th><th>version</th><th>etat</th><th>err level</th><th>durée</th><th>description</th><th>auteur</th> <th>git</th> </tr></thead>';
		foreach($this->libs as $key => $lib){
			//$lib= gestLib[index]
	//		$libNom=$lib->nom;
			$out.='<tr>';
			$out.='<td>'.$lib->nom.'</td>';
			$out.='<td>'.$lib->version.'</td>';
			$out.='<td>'.LEGRAL_LIBETAT_toString($lib->etat).'</td>';
			$out.='<td>'.LEGRALERR_toString($lib->err_level).'</td>';
			$out.='<td>'.$lib->getDuree().'</td>';
			$out.='<td>'.$lib->description.'</td>';
			$t=$lib->auteur;	$out.="<td><a target='exterieur' href='$lib->site'>$t</a></td>";
			$t=$lib->git;		$out.="<td><a target='git' href='$t'>$t</a></td>";
			$out.="</tr>\n";
			};

		$out.='</table>';
		return $out;
	}

	function libTableau(){return $this->tableau();}

	function setEtat($lib,$etat){$this->libs[$lib]->setEtat($etat);}
	function getDuree($lib)     {$this->libs[$lib]->getDuree();}
	function end($lib)          {$this->libs[$lib]->end();}
	function getLibError($lib)  {if(isset($this->libs[$lib]))return LEGRALERR_toString($this->libs[$lib]->err_level);}

	function debugShowOrigine($lib,$level,$commentaire='',$metaDatas='',$txt='',$format='br',$classe=''){
		$r ='<span class="gestLibVar">'.$lib.':'.$commentaire.':'.$metaDatas.'</span>';
		$r.='<div class="gestLib_inspectOrigine">'.$this->debugShow($lib,$level,$commentaire,$metaDatas,$txt,$format,$classe).'</div>';
		return $r;
	}
	function debugShow($lib,$level,$commentaire='',$metaDatas='',$txt='',$format='br',$classe=''){
		if(isset($this->libs[$lib]))return $this->libs[$lib]->debugShow($level,$commentaire,$metaDatas,$txt,$format,$classe);
		return '';
	}

	// - accee aux libs - //
	function debugShowVarOrigine($lib,$level,$commentaire='',$metaDatas='',$varNom='',$var='',$format='br',$classe=''){
		$r ='<span class="gestLibVar">'.$lib.':'.$commentaire.':'.$metaDatas.'</span>';
		$r.='<div class="gestLib_inspectOrigine">'.$this->debugShowVar($lib,$level,$commentaire,$metaDatas,$varNom,$var,$format,$classe).'</div>';
		return $r;
	}

	function debugShowVar($lib,$level,$commentaire='',$metaDatas='',$varNom='',$var='',$format='br',$classe=''){
		if(isset($this->libs[$lib]))return $this->libs[$lib]->debugShowVar($level,$commentaire,$metaDatas,$varNom,$var,$format,$classe);
		return '';
	}

}	// class gestionLibrairie

// - creation d'une instance prefedinie - //
$gestLib= new gestionLibrairies();
$gestLib->loadLib('gestLib',__FILE__,GESTLIBVERSION,'gestionnaire de librairies');
$gestLib->libs['gestLib']->git='https://git.framasoft.org/legraLibs/gestlib';
$gestLib->end('gestLib');

