<?php
error_reporting(E_ALL);
//error_reporting(1);
//init_set('display_errors','stdout');

define('VERSIONSTATIQUE',0); // attention est valide des que defini (meme si false)
if (defined('VERSIONSTATIQUE')){
	if (VERSIONSTATIQUE == 1){
		error_reporting(0);
		//display_errors(0);
		}
	}

ob_start();
// - docs communs- //
define('DOCUMENT_ROOT',$_SERVER['DOCUMENT_ROOT'].'/');
define('INTERSITES_ROOT','./');
define('_0PAGES_ROOT','/www/0pages/');
define('PAGES_ROOT','./pages/');
define('_0TEXTES_ROOT',DOCUMENT_ROOT.'0textes/');
define('_0ARTICLES_ROOT',DOCUMENT_ROOT.'0articles/');


// - docs specifiques auprojet - //
define('INDEX_ROOT','./');
define('MENU_ROOT','./menus/');
define('PAGESLOCALES_ROOT','./pagesLocales/');


//include (INTERSITES_ROOT.'lib/legral/php/intersites/intersites-3.0.php');
$lib='gestLib';

include ("./$lib.php");
//      $gestLib->libs[$lib]->setErr(LEGRALERR::DEBUG);

//*********************** ==== session ==== *****************
session_name('legral_session');session_start();

//*********************** ==== autorisation local ==== *****************

//*********** ==== Gestion des menus et pages  ==== *****************
$lib='menuStylisee';
include("./lib/legral/php/gestMenus/$lib.php");
//      $gestLib->libs[$lib]->setErr(LEGRALERR::DEBUG);

$lib='gestMenus';
include("./lib/legral/php/$lib/$lib.php");
//include("./lib/legral/php/$lib-v1.13.php");
//      $gestLib->libs[$lib]->setErr(LEGRALERR::DEBUG);

$gestMenus=new gestMenus('gestLib');
$gestMenus->metasPrefixes['title']='lib gestLibv'.$gestLib->libs['gestLib']->version.' - ';
include(MENU_ROOT.'menus-systeme.php');
include(MENU_ROOT.'menus-gestLib.php');

//*********************** ==== html ==== *****************
?><!DOCTYPE html><html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="robots" content="index,follow">
<meta name="author" content="Pascal TOLEDO">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="generator" content="vim">
<meta name="identifier-url" content="http://legral.fr">
<meta name="date-creation-yyyymmdd" content="20140804">
<meta name="date-update-yyyymmdd" content="20150416">
<meta name="reply-to" content="pascal.toledo@legral.fr">
<meta name="revisit-after" content="10 days">
<meta name="category" content="">
<meta name="publisher" content="legral.fr">
<meta name="copyright" content="pascal TOLEDO">
<?php $gestMenus->build();?>

<!-- scripts concatennes -->
<script src="./locales/scripts.js"> </script>



<!-- styles -->
<link rel="stylesheet" href="./styles/styles.css" media="all" />

</head>

<!-- body -->
<body>
<div id="page">

<!-- header -->
<div id="header">
<div id="headerGauche"><!--gauche--></div>
<h1><a href="http://legral.fr/">legral - lib PHP</a> : <a href="?<?php echo $gestMenus->menuDefaut?>">gestLib</a></h1>

<div id="headerDroit"><a href="?about=accueil">&agrave; propos de...</a></div>
</div><!-- header -->

<!-- menu + ariane + contenu page + ariane -->
<?php
echo '<div class="ariane">'.$gestMenus->ariane->showAriane().'</div>'."\n";

// - affiche les menus contruit en commencant par le menu indiquee par le constructeur. Inclu la page appellee - //
echo $gestMenus->show();

//include ('./piwik.php');
echo '<script>document.write(gestLib.tableau()); </script>';
echo $gestLib->tableau();

// -- ariane: affichage-- //
echo "\n".'<div class="ariane">'.$gestMenus->ariane->showAriane().'</div>'."\n";
include './footer.php';
?><!-- menu + ariane + contenu page + ariane : FIN -->


</div><!-- //page -->
</body></html>
