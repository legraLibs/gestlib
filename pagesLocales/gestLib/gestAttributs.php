<h1 class="h1">gestAttributs</h1>

<?php
$a=array();
/*
echo gestLib_inspect('$a',$a);
$a['b']='c';
echo gestLib_inspect('$a["b"]',$a["b"]);
echo gestLib_inspect('$a["n/a"]',$a["n/a"]);

unset($a);echo gestLib_inspect('$a',$a);
echo 'unset($NA);:';unset($NA);echo '<br>';// pas de notice d'erreur
echo '<br>';
 */

global $gestLib;
global $gestAttributs;
?>
<h2 class="h2">On crait la liste</h2>

<?php
$cmd=''."\n";
$cmd.='$liste=new gestAttributs();'."\n";
//$cmd.=''."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);
echo gestLib_inspect('$liste',$liste->get());
?>

<h2 class="h2">On remplit la liste</h2>
<?php
$cmd=''."\n";
$cmd.='$liste->set("nom","TOLEDO");'."\n";
$cmd.='$liste->set("prenom","Pascal");'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);

echo gestLib_inspect('$liste',$liste->get('nom'));		// affiche 1 attr
echo gestLib_inspect('$liste',$liste->get());			// affiche (sans param): tous les attr
echo gestLib_inspect('$liste->get("n/A")',$liste->get('n/A'));	// affiche un attr non exist
?>

<h2 class="h2">On unset un attr</h2>
<?php
$cmd=''."\n";
$cmd.='$liste->_unset("nom");'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);
echo gestLib_inspect('$liste->get()',$liste->get());
?>

<h2 class="h2">On unset la liste</h2>
<?php
$cmd=''."\n";
$cmd.='unset($liste);'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);
echo gestLib_inspect('$liste',$liste);
?>

