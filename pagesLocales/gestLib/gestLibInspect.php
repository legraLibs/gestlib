<?php function showCmd($cmd){	echo "<pre class='coding_code'>$cmd</pre>";}
global $gestLib,$gestMenus;
?>

<style>
h2.h2{
margin-top:1em;
}
</style>

<h1 class="h1">gestLib fonctions d'inspections inconditionnelles</h1>
<h2 class="h2">gestLib_inspect()</h2>
Cette fonction renvoie le type et la valeur d'une variable. Les éléments(dans le cas d'un type array ou object) la constituant sont également renvoyés de facon récursifs.<br>
L'affichage utilise la class .gestLib<br>

<?php
$cmd='$var=NULL; echo (gestLib_inspect(\'$var\',$var));'."\n"; echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);echo '<br>';
$cmd='$var=0; echo (gestLib_inspect(\'$var\',$var));'."\n"; echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);echo '<br>';
$cmd='$var=2; echo (gestLib_inspect(\'$var\',$var));'."\n"; echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);echo '<br>';
$cmd='$var="2"; echo (gestLib_inspect(\'$var\',$var));'."\n"; echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);echo '<br>';
$cmd='$var="texte"; echo (gestLib_inspect(\'$var\',$var));'."\n"; echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);echo '<br>';
echo '<br>';

$txt="gestLib_inspect est appellé hors ou à partir d'une fonction";
$cmd=''."\n";
$cmd='$txt="gestLib_inspect est appellé a partir d\'une fonction";'."\n";
$cmd.='echo (gestLib_inspect(\'$txt\',$txt));'."\n";
$cmd.='function callTxt(){;'."\n";
$cmd.='	global $txt;'."\n";
$cmd.='	echo(gestLib_inspect(\'$txt\',$txt));'."\n";
$cmd.='	echo(gestLib_inspect(\'$txt\',$txt,"commentaires",__CLASS__.\':\'.__FUNCTION__.\':\'.__LINE__,"br") );'."\n";
$cmd.='}'."\n";
$cmd.='callTxt();'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);
?>

<h2 class="h2">gestLib_inspectOrigine()</h2>
Cette fonction appelle gestLib_inspect en l'encadrant d'un &lt;div class="gestLib_inspectOrigine"&gt;  &lt;/div&gt;<br>
Elle sert quand la longeur de la sortie rend la lecture pénible.
<?php
$cmd=''."\n";
$cmd.='	echo gestLib_inspectOrigine(\'$gestMenus\',$gestMenus,"commentaires",__CLASS__.\':\'.__FUNCTION__.\':\'.__LINE__,"br");'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd);
?>

<h1 class="h1">gestLib fonctions d'inspections conditionnelles</h1>
Les fonctions suivantes seront affichés au non suivant le degré de debug configuré et demandé.<br>
L'indication de la lib fournit à qu'elle lib appartient le code.<br>
Les 2 fonctions existent également avec leur équivalent Origine ($gestLib-&gt;debugShowOrigine(), $gestLib-&gt;debugShowVarOrigine())<br>

<h2 class="h2">$gestLib-&gt;debugShow()</h2>
<?php
$cmd='$txt="Ce texte sera affiché si la lib est configuré au niveau:".LEGRALERR::ALWAYS;'."\n";
$cmd.='echo $gestLib->debugShow(\'gestMenus\',LEGRALERR::ALWAYS,"commentaires" ,__CLASS__.\':\'.__FUNCTION__.\':\'.__LINE__,$txt);'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd)	;
?>

<h2 class="h2">$gestLib-&gt;debugShowOrigine()</h2>
<?php
$cmd='$txt="Ce texte<br>est<br>sur<br>plusieurs<br>lignes<br>et<br>est<br>donc<br>tres<br>tres<br>tres<br>tres<br>tres<br>long";'."\n";
$cmd.='echo $gestLib->debugShowOrigine(\'gestMenus\',LEGRALERR::ALWAYS,"commentaires" ,__CLASS__.\':\'.__FUNCTION__.\':\'.__LINE__,$txt);'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd)	;
?>


<h2 class="h2">$gestLib-&gt;debugShowVar()</h2>
<?php
$cmd='$lib="Ma lib";'."\n";
$cmd.='	echo $gestLib->debugShowVar(\'gestMenus\',LEGRALERR::ALWAYS ,"commentaires" ,__CLASS__.\':\'.__FUNCTION__.\':\'.__LINE__,\'$lib\',$lib);'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd)	;
?>

<h2 class="h2">$gestLib-&gt;debugShowVarOrigine()</h2>
<?php
$cmd='$txt="Ce texte<br>est<br>sur<br>plusieurs<br>lignes<br>et<br>est<br>donc<br>tres<br>tres<br>tres<br>tres<br>tres<br>long";'."\n";
$cmd.='	echo $gestLib->debugShowVarOrigine(\'gestMenus\',LEGRALERR::ALWAYS ,"commentaires" ,__CLASS__.\':\'.__FUNCTION__.\':\'.__LINE__,\'$txt\',$txt);'."\n";
echo '<pre class="coding_code">'.htmlentities($cmd).'</pre>';eval($cmd)	;
?>

