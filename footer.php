<!-- footer -->
<!-- footer gauche-->
<div id="footer">
<div id="footerGauche">
<?php
define('DUREE',microtime(TRUE)-SCRIPTDEBUT);
if(( defined('VERSIONSTATIQUE')) AND (VERSIONSTATIQUE == 1) ){
    echo 'version statique(g&eacute;n&eacute;r&eacute;e le: '.date('d/m/Y').')';
}
else{
    echo 'page g&eacute;n&eacute;r&eacute; en '. number_format(DUREE,9).'s';
}
?></div><!-- footer gauche: fin-->

<!-- footer centre -->
<span class="licence">
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">
<img alt="Licence Creative Commons" src="./styles/img/licenceCCBY-88x31.png" /></a><br />Mise &agrave; disposition selon les termes de la<br> <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">licence Creative Commons Attribution 4.0 International</a>.<br />
<a xmlns:dct="http://purl.org/dc/terms/" href="http://legral.fr" rel="dct:source">http://legral.fr</a>.</span>

<!-- footer centre: fin -->

<!-- footer droit -->
<div id="footerDroit"><a href="?ksfv3=bugsToDo">Rapporter un bug</a><br>
<?php
if (ISDEV === 1){
    echo 'serveur reseau:'.SERVER_RESEAU.'<br>';
    echo 'serveur nom:'.SERVER_NAME.'<br>';
    echo 'IP:'.SERVER_ADDR.'<br>';
}
?>
</div><!-- footer droit: fin -->

</div><!-- footer -->
<?php
//php include './piwik.php';
?>
