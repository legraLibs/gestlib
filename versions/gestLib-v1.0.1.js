/*!
fichier: gestLib.js
auteur:pascal TOLEDo
date de creation : 2012.02.21
date d emodification: 2014.08.08
source: http://gitorious.org/gestLib
*/

GESTLIBVERSION= '1.0.1';

// gestionnaire de la box des donnees
function gestionLibrairie_dataBox(o){
//	this.nom=o.nom;		//facultatif
	this.idHTML=o.idHTML;	//facultatif
	this.idCSS=null;
	this.idCSSParent=o.idCSSParent;	// obligatoire
	this.className='gestLibDataBox';
	if(o.className)this.className+=' '+o.className;

	this.genre=(o.genre=='TEXTAREA')?'TEXTAREA':'DIV';	// 'TEXTAREA' sinon DIV par defaut
	return this;
}

gestionLibrairie_dataBox.prototype={
	create:function(){
		this.idCSS=document.createElement(this.genre.toLowerCase());
		this.idCSSParent.appendChild(this.idCSS);
		this.init();
		return this.idCSS;
	}
	// - 're)initialisation du contenaire - //
	,init:function(){
		this.idCSS.id=this.idHTML;
		this.idCSS.className=this.className;
		this.idCSS.style.display='block';
		return null;
	}
	,write:function(o){
		var str=o;	// par defaut o est supposé un string
		if(typeof(o)=='object'){
			if(this.genre=='TEXTAREA'){
				if(o.TEXTAREA)str=o.TEXTAREA;	// surcharge si specifier
				this.idCSS.value=str;
			}
			if(this.genre=='DIV'){
				if(o.DIV)str=o.DIV;	// surcharge si specifier
				this.idCSS.innerHTML=str;
			}
		}
		return null;
	}
}


//options: nom:obligatoire
//erreurNo:
// -1: absence de nom
// -10..-11..-12: absence de div pour la console
// 100+ specifique a la lib appellé
function gestionLibrairie_lib(options)
	{
	if((options.nom==undefined)&&((options.idHTML==undefined)))return null;	// l'un ou l'autre doit etre definit
	this.erreurNo=0;
	this.erreurTxt=new Array();//attention tableau de tableau
	if (options.nom==undefined){this.erreurNo=-1;}
	this.date=new Date();
	this.libType=(options.libType=='tiers')?'tiers':'perso';	// defaut:pas de console
	this.isConsole=(options.isConsole===1)?1:0;	// defaut:pas de console
	this.consoleGenre=null;	// TEXTAREA,DIV
	this.isVisible=(options.isVisible===0)?0:1;	// defaut:visible
	this.nom=   (options.nom   !=undefined)?options.nom   :options.idHTML;

	// - console - //
	this.idCSSSup=null;	this.idCSSNom=null;	this.idCSSTxt=null;
	this.idHTML=(options.idHTML!=undefined)?options.idHTML:options.nom;
	this.idHTMLMenus=this.idHTML+'_menus';
	this.idCSSMenus=null;	// support des menus
	this.dataBox=null;

	this.description=(options.description!=undefined)?options.description:'';
	this.ver=(options.ver!=undefined)?options.ver:0;
	this.url=(options.url!=undefined)?options.url:null;//site du code source
	this.deb=this.date.getTime();
	this.fin=null;	this.dur=null;
	//if (this.isConsole)	{this.setConsole();}

	this.instances=new Array();//liste des instances de la lib

	this.erreurTranslate();//en fin car neccesite variable

	return this; //renvoie un pointeur
	}

gestionLibrairie_lib.prototype=
	{
	erreurTranslate:function()
		{
		this.erreurTxt['fr']=new Array();
		this.erreurTxt['fr'][0]='ok';
		this.erreurTxt['fr'][1]='non charger';
		this.erreurTxt['fr'][-1]='nom non defini';
		this.erreurTxt['fr'][-10]='div '+this.idHTML+'_Support non defini';
		this.erreurTxt['fr'][-11]='span '+this.idHTML+'_Nom non defini';
		this.erreurTxt['fr'][-12]='div/textarea '+this.idHTML+'_Texte non defini';
		}
	,erreurShow:function(instNu,errNu,lang)
		{
		return this.erreurTxt[lang?lang:'fr'][errNu?errNu:this.erreurNo];
		}

	,destruct:function(){}
	,end:function(){this.date=new Date();this.fin=this.date.getTime();this.dur=this.fin-this.deb;}
	
	// ajoute le param ds la liste si object et renvoie 1 sinon eznvoie 0
	,instanceAdd:function(instPtr)
		{
		if(typeof(instPtr)==='object'){this.instances[this.instances.length]=instPtr;return 1;}
		return 0;
		}
	,ongletAdd:function(f){
		if(typeof(f)!='object')return null;
	
		// --  creation de la div this.creerCSS(); -- //
		var idCSS=document.createElement('span');
		this.idCSSMenus.appendChild(idCSS);

		// ==  initilialisation de creation == //
		idCSS.id=f.idHTML;
		idCSS.className="gestLibNom";
		idCSS.innerHTML=f.texte;
		//		idCSS.style.position='absolute';
//		idCSS.style.overflow='hidden';
//		idCSS.style.top=0;
//		idCSS.style.zIndex=this.zIndex;
//		idCSS.style.height =this.hauteur+'px';
		idCSS.style.cursor='pointer';
//		idCSS.style.opacity=this.opacity;
		return idCSS;
	}

	,setConsole:function(){
		// - support - //
		this.idCSSSup=document.getElementById(this.idHTML+'Console');	if (this.idCSSSup==null){this.erreurNo=-10;return null;}
		this.consoleGenre=this.idCSSSup.tagName.toUpperCase(); // DIV, TEXTAREA
	


		// - creation du support des menus - //
		this.idCSSMenus=document.createElement('div');
		this.idCSSMenus.id=this.idHTMLMenus;	// utile?
		this.idCSSSup.appendChild(this.idCSSMenus); 

		// -- onglet:nom(show|hide) -- //
		var f={"idHTML":this.idHTML+"_nom","texte":"console:"+this.nom};
		this.idCSSNom=this.ongletAdd(f);
		this.idCSSNom.onclick=function(elt){
			var CSsId=elt.currentTarget.parentElement.parentElement.children[1].style; // attention ordre de l'elt code en DUR!!!
			CSsId.display=(CSsId.display=='block')?'none':'block';
		}


		// -- onglet:clear(efface le contenu) -- //
		var f={"idHTML":this.idHTML+"_clear","texte":"clear"};
		this.idCSSClr=this.ongletAdd(f);
		this.idCSSClr.onclick=function(elt){
			var CSSId=elt.currentTarget.parentElement.parentElement.children[1]; // atention ordre de l'elt code en DUR!!!
			switch (CSSId.tagName.toUpperCase()){
				case 'TEXTAREA':CSSId.value="";break;
				case 'DIV':default:CSSId.innerHTML="";break;
			}
		}

		// - creation de la dataBox - //
		this.dataBox=new gestionLibrairie_dataBox({"idCSSParent":this.idCSSSup});
		this.dataBox.create();
		//		this.idCSSTxt=document.getElementById(this.idHTML+'Texte');	if (this.idCSSTxt==null){this.erreurNo=-12;}


		if (!this.erreurNo){this.idCSSNom.innerHTML='console:'+this.nom;}
		this.dataBox.write("console activée");
		if (!this.isVisible){this.hide();}
		this.isConsole=1;
	}

	,valueToString:function(val)
			{
			if (val===undefined){return"'undefined'";}
			if (val===null){return"'null'";}
			return val;
			}
	,evaluer:function()
		{
		if (this.idCSSTxt)
			{switch (this.idCSSTxt.tagName.toUpperCase())
				{
				case 'TEXTAREA':eval(this.idCSSTxt.value);break;
				case 'DIV':default:eval(this.idCSSTxt.innerHTML);
				break;
				}
			}
		}
	,clear:function()
		{
		if (this.idCSSTxt)
			{
			this.consoleTexte='';
			//this.consoleTexte='console Clear<br>';
			switch (this.idCSSTxt.tagName.toUpperCase())
				{
				case 'TEXTAREA':this.idCSSTxt.value='';break;
				case 'DIV':default:this.idCSSTxt.innerHTML=this.consoleTexte;
				break;
				}
			}
		}
	,display:function(etat){if (this.idCSSSup!=null){if (etat==undefined){etat=1;};this.idCSSSup.style.display=(etat==1)?'block':'none';}}
	,switchShow:function(){if (this.idCSSTxt!=null){if(this.isVisible){this.hide();}else{this.show();}}}
	,show:function(){if (this.idCSSTxt!=null){this.isVisible=1;this.idCSSTxt.style.display='block';}}
	,hide:function(){if (this.idCSSTxt!=null){this.isVisible=0;this.idCSSTxt.style.display='none';}}
	,write:function(o){this.dataBox.write(o);}
/*	,write:function(txt,br)
		{
		if(this.idCSSTxt!==null)
			{var BR=br?br:'';
			switch (this.idCSSTxt.tagName.toUpperCase())
				{
				case 'TEXTAREA':
					BR=BR?BR:'\n';
					this.consoleTexte+=txt+BR;
					if (this.idCSSTxt){this.idCSSTxt.value=this.consoleTexte;}
					break;
				case 'DIV':default:
					BR=BR?BR:'<br>';
					this.consoleTexte+=txt+BR;
					if (this.idCSSTxt){this.idCSSTxt.innerHTML=this.consoleTexte;}
					break;
				}
			}
		return 0;
		}
*/		
	//inspect renvoie un texte avec le nom et la valeur d'une variable DANS la console
	,inspect:function(o){
		if(typeof(o)!='object')return null;
		this.dataBox.write({
			 "TEXTAREA":o.varNom+'='+o.varPtr+'\n'
			,"DIV":'<span class="gestLibVarNom">'+o.varNom+'</span>=<span class="gestLibVar">'+o.varPtr+'</span><br>'
		});
	}
	,inspectAll:function(varNom,varPtr)
		{
		var out='INSPECT';
		if (varNom){out+='<span>'+varNom+'=</span>';}
		out+='<ul>';
		for (value in varPtr){out+='<li>'+this.inspectAll(value+'</li>');}
		out+='</ul>';
		if (varNom){out+='</span><br>';}
		this.consoleTexte+=out;
		if (this.idCSSTxt){this.idCSSTxt.innerHTML=this.consoleTexte;}
		}


	}//gestionLibrairie_lib.prototype

/* ************************************************************
// ---- gestionLibrairie ----
   ***********************************************************/
function gestionLibrairie()
	{
	this.erreurNo=0;this.erreurTxt=new Array();//attention tableau de tableau
	this.libNb=0;this.libs=new Array();
	return this;}
gestionLibrairie.prototype=
	{
	////////////////////////////////
	//Gestion des erreurs
	erreurTranslate:function()
		{
		this.erreurTxt['fr']=new Array();
		this.erreurTxt['fr'][0]='ok';
		}
	,erreurShow:function(errNu,lang)
		{
		var out='';
		for(libNu in this.libs)
			{
			if(typeof(this.libs[libNu].erreurShow)==='function')
				{
				out+=this.libs[libNu].nom+':'+this.libs[libNu].erreurShow(errNu,lang)+' ';
				}
			}
		return out;
		}

	////////////////////////////////
	,instanceAdd:function(lib,instPtr)
		{
		if(this.libs[lib])this.libs[lib].instanceAdd(instPtr);
		}
		
	////////////////////////////////
	,destruct:function(lib)
		{
		document.write('destruct:function(lib)<br>');
		if(lib)
			{
			if(this.libs[lib])
				{
				//document.write('===>this.libs[lib]:'+this.libs[lib]+'<br>');
				//this.libs[lib].destruct();
				this.libs[lib]=undefined;
				this.libNb--;
				}
			}
		else	{
			var libNu=0;
			for(libr in this.libs)
				{
//				document.write('<b>===->this.libs[libr].nom:'+this.libs[libr].nom+'</b><br>');
//				document.write('===>this.libs[libr]:'+this.libs[libr]+'<br>');
//				document.write('=== >this.libs[libr].typeof:'+this.libs[libr].typeof+'<br>');
//				this.libs[libr].destruct();
				this.libs[libr]=undefined;
				libNu++;
				if (libNu>=this.libNb){break;}
				}
			this.libs=undefined;
			this.libNb=0;
			}
		},

	////////////////////////////////
	//gestion des consoles
	end:function(libNom)
		{if (libNom&&this.libs[libNom]){this.libs[libNom].end();}},
	setConsole:function(libNom)
		{if (libNom&&this.libs[libNom]){this.libs[libNom].setConsole();}},
	clear: function(libNom)
		{if (libNom&&this.libs[libNom]&&this.libs[libNom].isConsole){this.libs[libNom].clear();}},
	evaluer:function(options)
		{if(options&&this.libs[options.lib]&&this.libs[options.lib].isConsole){this.libs[options.lib].evaluer();}},

	write: function(options,br)
		{if (options&&this.libs[options.lib]&&this.libs[options.lib].isConsole){this.libs[options.lib].write(options.txt,br);}},

	inspect:function(o)
		{var lib=this.libs[o.lib];if(o && lib && lib.isConsole){lib.inspect(o);}},
	inspectAll:function(o)
		{var lib=this.libs[options.lib];if(o && lib && lib.isConsole){lib.inspectAll(o);}},

	loadLib:function(options)
		{
		if (!options){return -1;}
		if (!options.nom){return -2;}
		if(this.libs[options.nom]==undefined ||options.force)
			{
			this.libs[options.nom]=new gestionLibrairie_lib(options);
			if (this.libs[options.nom].erreurNo===0){return this.libs[options.nom];}
			this.libNb++;
			return 0;		
			}
		return -3;
		},
	display:function(libNom,etat){if(this.libs[libNom].isConsole){this.libs[libNom].display(etat);}},
	switchShow:function(libNom){if(this.libs[libNom].isConsole){this.libs[libNom].switchShow();}},
	show:function(libNom){if(this.libs[libNom].isConsole){this.libs[libNom].show();}},
	hide:function(libNom){if(this.libs[libNom].isConsole){this.libs[libNom].hide();}},

	////////////////////////////////
	tableau:function()
		{
		var out='';
		out+='<table class="gestLib"><caption>Librairies JavaScript ('+this.libNb+')</caption>';
		out+='<thead><tr><th>type</th><th>nom</th><th>version</th><th>err</th><th>dur&eacute;e</th>';
		out+='<th>description</th><th>url</th>';
		out+='<th title="console demander?">console</th><th title="console visible?">visible</th><th>idHTML</th><th>instances</th></tr></thead>';

		for(value in this.libs)
			{if (this.libs[value].nom)
				{
				url='';
				if (this.libs[value].url!=null){url='<a target="git" href="'+this.libs[value].url+'">lien</a>';}
				out+='<tr><td>'+this.libs[value].libType+'</td><td>'+this.libs[value].nom+'</td><td>'+this.libs[value].ver+'</td>';
				out+='<td>'+this.libs[value].erreurNo+':'+this.libs[value].erreurShow()+'</td>';
				out+='<td>'+this.libs[value].dur+'</td>';
				out+='<td>'+this.libs[value].description+'</td><td>'+url+'</td>';
				out+='<td>'+this.libs[value].isConsole+'</td><td>'+this.libs[value].isVisible+'</td><td>'+this.libs[value].idHTML+'</td>';
				out+='<td>';
	
				//erreurs liee  aux instances
				var instout='';
				for(instNu in this.libs[value].instances)
					{instout+='<b>'+instNu+':</b>';
					if(typeof(this.libs[value].instances[instNu].erreurShow)==='function')
						{instout+=this.libs[value].instances[instNu].erreurShow();}
					else	{instout+='pas de function erreurShow()';}
					instout+='<br>';
					}
				if(instout!='')out+=instout;
				out+='</td>';
				out+='</tr>\n';
				}
			};
		out+='</table>';
		return out;
		}
	} //class gestionLibrairie prototype

/* ************************************************************
instantiation du gestionnaire
 ***********************************************************/
gestLib=new gestionLibrairie();
gestLib.loadLib({nom:'gestLib'/*,idHTML:'gestLib'*/,ver:GESTLIBVERSION,description:'gestionnaire de librairies js',isConsole:0,isVisible:0,url:'http://gitorious.org/gestLib'});
gestLib.instanceAdd('gestLib',gestLib);

gestLib.end('gestLib');
