<?php
// ==== menu: gestLib ==== //
$mn='gestLib';
$pagePath=PAGESLOCALES_ROOT."$mn/";
$p='accueil';
$m=$gestMenus->addMenu($mn,$p,$pagePath.'accueil.html');
	// -- parametrer la page -- //
	$m->setAttr($p,'visible',1);				// 0: le li ne sera pas affiche 1:afficher
	$m->setAttr($p,'menuTitre','lib:gestLib');		// afficher dans l'onglet (s'il correspond a un memnu , celui ci sera appelle) 
	$m->setAttr($p,'menuTitle','lib:gestLib');		// afficher au survol du titre (ariane et onglet) si pas defini alors menuTitle=menuTitre
	$m->setAttr($p,'titre','librairie gestLib');		// titre de la page: afficher dans le bas de page
//        $m->setMeta($p,'title','tutoriels - accueil(meta)');	// meta <title> (si non definit title=titre)
//	$m->addCssA($p,'dossier1');                          // applique le style dossier1 a la balise <a>

$p='gestLibInspect';
$m->addCallPage($p,$pagePath.$p.'.php');
        $m->setAttr("$p",'menuTitre',$p);
        $m->setAttr("$p",'titre',$p);

$p='gestTextes';
$m->addCallPage($p,$pagePath.$p.'.php');
        $m->setAttr("$p",'menuTitre',$p);
        $m->setAttr("$p",'titre',$p);
//        $m->addCssA("$p",'dossier1');

$p='gestAttributs';
$m->addCallPage($p,$pagePath.$p.'.php');
        $m->setAttr("$p",'menuTitre',$p);
        $m->setAttr("$p",'titre',$p);

?>
